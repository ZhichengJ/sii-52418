// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include <string>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"
#include <pthread.h>

using namespace std;
class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value, float temporizador);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera, esfera2;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	string logger;

	DatosMemCompartida datos_mem;
	DatosMemCompartida *pdatos_mem;
	char *proyeccion;
	
	//para registrar si el jugador 1 ha movido o no
	bool juego;
	float temp_juego;
	int fd;
	int fd2;
	int fd3;
	int status;
	int puntos1;
	int puntos2;
  	int puntos1_prev;
	int puntos2_prev;
	pthread_t thid1;
	char tecla[6]="tecla";
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
