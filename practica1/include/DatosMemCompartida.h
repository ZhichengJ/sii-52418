#pragma once
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida{
public:
	Esfera esfera1;
	Esfera esfera2;
	Raqueta raqueta1;
	Raqueta raqueta2;
	int accion1, accion2;//1 arriba, 0 nada, -1 abajo
	//indica si el juego tenis se ha terminado
	bool fin;
};
