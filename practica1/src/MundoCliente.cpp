// Mundo.cpp: implementation of the CMundo class.
//AUTOR: ZHICHENG JIANG
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente(){
	char datos[100];
	//Cerrar la proyeccion de memoria
	//Enviar señal apagado a Bot
	pdatos_mem->fin = true;
	munmap(proyeccion,sizeof(datos));
	//Cerrar la tuberia
	if(unlink(coord)==-1){
		perror("unlink coordenadas:");
		exit(-1);
	}
	if(unlink(tecla)==-1){
		perror("unlink tecla:");
		exit(-1);
	}
	//Cerrar el fd2 y fd3
	if(close(fd2)==-1){
		perror("close coordenadas:");
		exit(-1);
	}
	if(close(fd3)==-1){
		perror("close tecla:");
		exit(-1);
	}
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print_servidor(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print_servidor(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print_servidor(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	esfera2.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value, float temporizador)
{	
	//-----Bot--------
	//Actualizacion de los valores de la memoria compartida
	pdatos_mem->esfera1=esfera;
	pdatos_mem->raqueta1=jugador1;
	pdatos_mem->esfera2=esfera2;
        pdatos_mem->raqueta2=jugador2;
	switch(pdatos_mem->accion1){
	case 1:
		CMundoCliente::OnKeyboardDown('w',0,0);
		break;
	case -1:
		CMundoCliente::OnKeyboardDown('s',0,0);
		break;
	}
	if(temp_juego<=0.0F){
		switch(pdatos_mem->accion2){
		case 1:
			CMundoCliente::OnKeyboardDown('o',0,0);
			break;
		case -1:
			CMundoCliente::OnKeyboardDown('l',0,0);
			break;
		}	
	}
	//Desactivo Bot2
	/*
	if(!juego)
		temp_juego-=0.025F;
		*/
	//Recogida de datos de coordenadas pasados por fifo
	char coordenadas[200];
	int b_leidos = read(fd2,coordenadas,sizeof(coordenadas));
	if(b_leidos == -1){
		perror("read coord");
		exit(-1);
	}
	else if(coordenadas[0]!='f'){
		sscanf(coordenadas,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d", &esfera.radio, &esfera2.radio, &esfera.centro.x,&esfera.centro.y, &esfera2.centro.x, &esfera2.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 
	}
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char tecla[20];
	int n;
	switch(key)
	{
	case 's':
	//jugador1.velocidad.y=-4;
	n=sprintf(tecla,"s");
	break;
	case 'w':
	//jugador1.velocidad.y=4;
	n=sprintf(tecla,"w");
	break;
	case 'l':
	//jugador2.velocidad.y=-4;
	n=sprintf(tecla,"l");
	juego=true;
	break;
	case 'o':
	//jugador2.velocidad.y=4;
	n=sprintf(tecla,"o");
	juego=true;
	break;
	//No mover en caso de pulsar el resto de teclas
	default:
	n=sprintf(tecla,"n");
	}
	//Escribir en tecla la tecla pulsada
	int b_escritos = write(fd3,tecla,n+1);
	if(b_escritos == -1){
		perror("write tecla");
		exit(-1);
	}
	else if(b_escritos != (n+1)){
		printf("WRITE tecla: No se ha escrito los mismos bytes que se ha pedido\n");
	}	
}

void CMundoCliente::Init()
{
	//Nombre del fichero creado por FIFO
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);
	
	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1.0F;
	jugador1.x2=-6;jugador1.y2=1.0F;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1.0F;
	jugador2.x2=6;jugador2.y2=1.0F;
	
	esfera2.radio = 0.0F;
	esfera.centro.y = 1.0F;
	esfera2.centro = 2.0F;
	esfera.radio = 0.5F;
	esfera.velocidad = 0;
	esfera2.velocidad = 0;
	//Proyeccion de memoria compartida
	//Mundo se abre antes que Bot por lo que se encarga de crear el fichero
	int fd_MC=open("Bot.txt",O_RDWR|O_CREAT|O_TRUNC,0666);
	ftruncate(fd_MC,sizeof(datos_mem));
	proyeccion=(char*)mmap(NULL,sizeof(datos_mem),PROT_WRITE|PROT_READ,MAP_SHARED,fd_MC,0);
	close(fd_MC);
	pdatos_mem=(DatosMemCompartida*) proyeccion;
	//Inicializar la accion a 0
	pdatos_mem->accion1 = 0;
	pdatos_mem->accion2 = 0;
	pdatos_mem->fin = false;
	//Cronometro inicializado a 10
	temp_juego = 10.0F;
	//Creacion tuberia de coordenadas
	if(mkfifo(coord,0777)==-1){
		perror("mkfifo coord:");
		exit(-1);
	}
	//Apertura del FIFO coordenadas en modo lectura
	fd2 = open(coord,O_RDONLY);
	if(fd2 == -1){
		perror("open coordenadas:");
		exit(-1);
	}
	//Creacion de la tuberia de envio de teclas
	if(mkfifo(tecla,0777)==-1){
		perror("mkfifo tecla:");
		exit(-1);
	}
	//Apertura del FIFO tecla en modo escritura
	fd3=open(tecla,O_WRONLY);
	if(fd3 == -1){
		perror("open tecla:");
		exit(-1);
	}
}
