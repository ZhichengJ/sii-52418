#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>

int main (void){
	char datos[100];
	int salir = 0;
	char log[]="logger";
	//Creación del FIFO
	if(mkfifo("logger",0777)==-1){
		perror("mkfifo");
		exit(-1);
	}
	//Abrir fifo en modo escritura
	int fd;
	fd = open("logger",O_RDONLY);
	//Lectura de los datos
	while(salir==0){
		if(read(fd, datos, sizeof(datos))==0)
			salir = 1;
		else
			printf("%s",datos);
	}
	unlink("logger");
	close(fd);
}
