// Mundo.cpp: implementation of the CMundo class.
//AUTOR: ZHICHENG JIANG
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/mman.h>
#include "MundoServidor.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

//funcion a ejecutar del thread
void* hilos_comandos(void *d){
	CMundoServidor* p=(CMundoServidor*) d;
	p->RecibeComandosJugador();
}

CMundoServidor::~CMundoServidor(){
	//Enviar apagado al cliente
	char cad[10];
	int n = sprintf(cad,"f");
	write(fd2,cad,n+1);
	//Cerrar fd de logger y fd2 de coordenadas
	if(close(fd)==-1){
		perror("close logger:");
		exit(-1);
	}
	if(close(fd2)==-1){
		perror("close coordenadas:");
		exit(-1);
	}
	if(close(fd3)==-1){
		perror("close tecla");
		exit(-1);
	}
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	esfera2.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value, float temporizador)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera2.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(esfera2);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	if(esfera.radio >= 1&&esfera.radio<0.2)
		esfera.radio-=0.1;

	if(esfera2.radio >= 1&&esfera.radio<0.2)
		esfera2.radio-=0.1;


	if(jugador1.Rebota(esfera)){
		if(esfera.radio>0.2)
			esfera.radio/=1.4;
		if(abs(jugador2.y2 - jugador2.y1) >= 1.2f){
			jugador2.y1*=0.8;
			jugador2.y2*=0.8;
		}
	}
	if(jugador2.Rebota(esfera)){
		if(esfera.radio>0.2)			
			esfera.radio/=1.4;
		if(abs(jugador1.y2 - jugador1.y1) >= 1.2F){
			jugador1.y1*=0.8;
			jugador1.y2*=0.8;
		}
	}
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}

	if(jugador1.Rebota(esfera2)){
		if(esfera2.radio>0.2)		
			esfera2.radio/=1.4;
		if(jugador2.y2 - jugador2.y1 > 0.5){
			jugador2.y1*=0.8;
			jugador2.y2*=0.8;
		}
	}
	if(jugador2.Rebota(esfera2)){
		if(esfera2.radio>0.2)			
			esfera2.radio/=1.4;
		if(jugador1.y2 - jugador1.y1 > 0.5){
			jugador1.y1*=0.8;
			jugador1.y2*=0.8;
		}
	}
	if(fondo_izq.Rebota(esfera2))
	{
		esfera2.centro.x=0;
		esfera2.centro.y=rand()/(float)RAND_MAX;
		esfera2.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera2.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera2))
	{
		esfera2.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}
	if(temporizador >=4.0F && esfera2.radio == 0.0F){
		esfera2.radio = 0.5F;
		esfera2.centro.x=0.0F;
		esfera2.centro.y=0.0F;
	}
	//-----Logger-------
	//Enviar datos cuando produce cambio
	char datos[100];
	if(puntos1_prev != puntos1){
		int n = sprintf(datos,"Jugador 1 marca 1 punto, lleva un total de %d puntos\n",puntos1);
		if(write(fd,datos,n+1)==-1){
			perror("write");
		}
	}
	if(puntos2_prev != puntos2){
		int n = sprintf(datos,"Jugador 2 marca 1 punto, lleva un total de %d puntos\n",puntos2);
                if(write(fd,datos,n+1)==-1){
                        perror("write");
                }
 
	}
	//actualización de los puntos de cada jugador
	puntos1_prev = puntos1;
	puntos2_prev = puntos2;
	
	//-------Envio de coordenadas a cliente-------
	char coordenadas[200];
	int n = sprintf(coordenadas,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d",esfera.radio, esfera2.radio, esfera.centro.x,esfera.centro.y, esfera2.centro.x, esfera2.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);

        int bytes_escritos = write(fd2,coordenadas, n + 1);
	if(bytes_escritos==-1){
		perror("write coordenadas:");
		exit(-1);
	}
	else if(bytes_escritos!=n+1)
		printf("Write coordenadas: Los bytes escritos no coinciden con los que se desea escribir.\n");
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	
	switch(key)
	{
	case 's':
	jugador1.velocidad.y=-4;
	break;
	case 'w':
	jugador1.velocidad.y=4;
	break;
	case 'l':
	jugador2.velocidad.y=-4;
	break;
	case 'o':
	jugador2.velocidad.y=4;
	break;
	}
}

void CMundoServidor::Init()
{
	//Nombre del fichero creado por FIFO
	char log[]="logger";
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);
	
	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1.0F;
	jugador1.x2=-6;jugador1.y2=1.0F;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1.0F;
	jugador2.x2=6;jugador2.y2=1.0F;
	
	esfera2.radio = 0.0F;
	esfera.centro.y = 1.0F;
	esfera2.centro = 2.0F;
	esfera.radio = 0.5F;
	//Abre la tuberia en modo escritura
	fd=open("logger",O_WRONLY);
	puntos1_prev=0;
	puntos2_prev=0;
	//Apertura del FIFO
	fd2=open("coord",O_WRONLY);
	if(fd2==-1){
		perror("open coord");
		exit(-1);
	}
	fd3=open(tecla,O_RDONLY);
	if(fd3==-1){
		perror("open tecla");
		exit(-1);
	}
	//Creación del thread
	pthread_create(&thid1,NULL,hilos_comandos,this);
}



void CMundoServidor::RecibeComandosJugador(){
	while(1){
            usleep(10);
	    char cad[100];
            read(fd3, cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
	    if(key=='n')jugador2.velocidad.y=0;
	    if(key=='F')exit(0);
      }
}


