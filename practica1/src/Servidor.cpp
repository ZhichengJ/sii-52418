#include "glut.h"
#include "MundoServidor.h"
#include <signal.h>
#include <sys/wait.h>

CMundoServidor mundo;
float temporizador = 0.0F;
//los callback, funciones que seran llamadas automaticamente por la glut
//cuando sucedan eventos
//NO HACE FALTA LLAMARLAS EXPLICITAMENTE
void OnDraw(void); //esta funcion sera llamada para dibujar
void OnTimer(int value); //esta funcion sera llamada cuando transcurra una temporizacion
void OnKeyboardDown(unsigned char key, int x, int y); //cuando se pulse una tecla	

void tratar_senyar(int n){
	printf("El proceso se ha terminado correctamente.\n");
	exit(0);
}
void estado(int n){
	mundo.~CMundoServidor();
	//printf("El proceso se ha terminado con estado de terminación %d.\n",WEXITSTATUS(status));
}

int main(int argc,char* argv[])
{
	//Señales
	struct sigaction act1, act2;
	act1.sa_handler = &tratar_senyar;
	act1.sa_flags = SA_RESTART;
	sigaction(SIGUSR1,&act1,NULL);
	act2.sa_handler = &estado;
	act2.sa_flags = SA_RESTART;
	sigaction(SIGINT,&act2,NULL);
	sigaction(SIGTERM,&act2,NULL);
	sigaction(SIGPIPE,&act2,NULL);
	
	//Inicializar el gestor de ventanas GLUT
	//y crear la ventana
	glutInit(&argc, argv);
	glutInitWindowSize(800,600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("MundoServidor");


	//Registrar los callbacks
	glutDisplayFunc(OnDraw);
	//glutMouseFunc(OnRaton);

	glutTimerFunc(25,OnTimer,0);//le decimos que dentro de 25ms llame 1 vez a la funcion OnTimer()
	glutKeyboardFunc(OnKeyboardDown);
	glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);
	
	mundo.InitGL();
	
	//pasarle el control a GLUT,que llamara a los callbacks
	glutMainLoop();
	
	return 0;   
}

void OnDraw(void)
{
	mundo.OnDraw();
}
void OnTimer(int value)
{	
	temporizador += 0.025F;
	mundo.OnTimer(value, temporizador);
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
}
void OnKeyboardDown(unsigned char key, int x, int y)
{
	mundo.OnKeyboardDown(key,x,y);
	glutPostRedisplay();
}
