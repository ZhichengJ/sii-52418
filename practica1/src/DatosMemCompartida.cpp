#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <cmath>

#include "../include/DatosMemCompartida.h"
#include "../include/Esfera.h"
#include "../include/Raqueta.h"

int main(void){
	int fd;
	DatosMemCompartida *pdatos_mem;
	char* proyeccion;

	//Se abre el fichero
	fd=open("Bot.txt",O_RDWR);
	ftruncate(fd,sizeof(DatosMemCompartida));
	//Proyeccion mediante mmap
	proyeccion = (char *)mmap(NULL,sizeof(DatosMemCompartida),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);

	//Se cierra el fichero
	close(fd);
	
	//apuntar pdatos_mem a la proyeccion
	pdatos_mem = (DatosMemCompartida*)proyeccion;
	
	//accion de la raqueta segun su posicion con respecto con la esfera
	while(1){
		usleep(25000);
		float posRaqueta1;
		float posRaqueta2;
		//posicion de la raqueta como el centro de la raqueta
		posRaqueta1=(pdatos_mem->raqueta1.y2+pdatos_mem->raqueta1.y1)/2;
		posRaqueta2=(pdatos_mem->raqueta2.y2+pdatos_mem->raqueta2.y1)/2;
		//Ver si la raqueta se situa por encima o por debajo de la esfera
		Vector2D *dir;
		//Se calcula la distancia de las esferas con la raqueta
		float dist11=abs(pdatos_mem->raqueta1.x1-pdatos_mem->esfera1.centro.x);
		float dist12=abs(pdatos_mem->raqueta1.x1-pdatos_mem->esfera2.centro.x);
		float dist21=abs(pdatos_mem->raqueta2.x1-pdatos_mem->esfera1.centro.x);
		float dist22=abs(pdatos_mem->raqueta2.x1-pdatos_mem->esfera2.centro.x);
		if(dist11<=dist12){
			if(posRaqueta1<pdatos_mem->esfera1.centro.y)
				pdatos_mem->accion1=1;
			else if(posRaqueta1>pdatos_mem->esfera1.centro.y)
        	                pdatos_mem->accion1=-1;
			else
				pdatos_mem->accion1=0;
		}
		else{
			if(posRaqueta1<pdatos_mem->esfera2.centro.y)
                                pdatos_mem->accion1=1;
                        else if(posRaqueta1>pdatos_mem->esfera2.centro.y)
                                pdatos_mem->accion1=-1;
                        else
                                pdatos_mem->accion1=0;
		}
		if(dist21<=dist22){
			if(posRaqueta2<pdatos_mem->esfera1.centro.y)
                                pdatos_mem->accion2=1;
                        else if(posRaqueta2>pdatos_mem->esfera1.centro.y)
                                pdatos_mem->accion2=-1;
                        else
                                pdatos_mem->accion2=0;

		}
		else{
			if(posRaqueta2<pdatos_mem->esfera2.centro.y)
                       		pdatos_mem->accion2=1;
 	              	else if(posRaqueta2>pdatos_mem->esfera2.centro.y)
        	               	pdatos_mem->accion2=-1;
        	       	else
                	       	pdatos_mem->accion2=0;
		}
		//Comprobar si el juego ha terminado
		if(pdatos_mem->fin)
			break;
	}
	pdatos_mem->accion1=0;
	pdatos_mem->accion2=0;
	munmap(proyeccion,sizeof(*(pdatos_mem)));
}
